from django.db import models


class Person(models.Model):
    """
    Модель человека для 1 лабы (имя и возраст)
    """
    name = models.CharField(max_length=64, null=False)
    age = models.IntegerField(null=False)

    def __str__(self):
        return f'{self.name}, age {self.age}'
