from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^persons/', views.PersonsView.as_view()),
]
