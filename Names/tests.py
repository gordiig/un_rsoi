from TestUtils.models import BaseTestCase
from .models import Person


class PersonsViewTestCase(BaseTestCase):
    """
    Тесты для PersonsView
    """
    def setUp(self):
        super().setUp()
        self.url = self.url_prefix + 'persons/'
        self.person_name = 'Test_name'
        self.person_age = 20
        self.person = Person.objects.create(name=self.person_name, age=self.person_age)

    def test_get_list(self):
        response_json = self.get_response_and_check_status(url=self.url)
        try:
            json_person = list(filter(lambda x: x['name'] == self.person_name, response_json))[0]
        except IndexError:
            self.assertTrue(False, msg='No person in db with needed name, but should be')
        try:
            self.assertTrue(json_person['name'] == self.person_name, msg='Response json name is not equal to test case '
                                                                         'person')
            self.assertTrue(json_person['age'] == self.person_age, msg='Response json age is not equal to test case '
                                                                       'person')
        except KeyError:
            self.assertTrue(False, msg='Response json doesn\'t have needed fields')

    def test_get_queryparams_404(self):
        url_404 = self.url + f'?name={self.person_name + "Nah"}'
        _ = self.get_response_and_check_status(url=url_404, expected_status_code=404)

    def test_get_queryparams_400(self):
        url_400 = self.url + '?age=21'
        _ = self.get_response_and_check_status(url=url_400, expected_status_code=400)

    def test_get_queryparams_200(self):
        url_200 = self.url + f'?name={self.person_name}'
        response_json = self.get_response_and_check_status(url=url_200)
        try:
            self.assertTrue(response_json[0]['name'] == self.person_name, msg='Response json name is not equal to test '
                                                                              'case person')
            self.assertTrue(response_json[0]['age'] == self.person_age, msg='Response json age is not equal to test '
                                                                            'case person')
        except KeyError:
            self.assertTrue(False, msg='Response json doesn\'t have needed fields')
