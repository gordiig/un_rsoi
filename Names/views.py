from rest_framework import status
from rest_framework.views import APIView, Request, Response
from .models import Person
from .serializers import PersonSerializer


class PersonsView(APIView):
    """
    Вью для списка людей
    """
    def get(self, request: Request):
        if len(request.query_params) != 0:
            name = request.query_params.get('name')
            if name is None:
                return Response({'error': 'No "name" field in query params'}, status=status.HTTP_400_BAD_REQUEST)
            to_serialize = Person.objects.filter(name=name)
            if len(to_serialize) == 0:
                return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            to_serialize = Person.objects.all()
        serializer = PersonSerializer(to_serialize, many=True, context={'request': request})
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request: Request):
        serializer = PersonSerializer(data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
